import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Test from "./Test.jsx";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

ReactDOM.render(<Test />, document.getElementById("root"));
