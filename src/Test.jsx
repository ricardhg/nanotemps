import React from "react";

import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';

import Home from './Home';
import Pagina1 from './Previsio';
import Pagina2 from './Persones';
import Pagina3 from './Pagina3';
import Pagina4 from './Pagina4';
import Detalle from './Detalle';
import NotFound from './NotFound';

export default () => (
    <BrowserRouter>
        <Container>
            <Row>
                <Col xs="3">
                    <ul>
                        <li> <Link to="/">Home</Link> </li>
                        <li> <Link to="/inicio">Pagina1</Link> </li>
                        <li> <Link to="/pagina2">Pagina2</Link> </li>
                        <li> <Link to="/pagina3">Pagina3</Link> </li>
                        <li> <Link to="/pagina4">Pagina4</Link> </li>
                    </ul>
                </Col>
                <Col xs="8">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/inicio" component={Pagina1} />
                        <Route path="/pagina2" component={Pagina2} />
                        <Route path="/pagina3" component={Pagina3} />
                        <Route path="/pagina4" render={() => <Pagina4 xx="algo" />} />
                         <Route path="/detalle/:idPersona" component={Detalle} />
      
                        <Route component={NotFound} />
                    </Switch>
                </Col>
            </Row>



        </Container>
    </BrowserRouter>
);
