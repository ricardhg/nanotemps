import React from "react";


import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import {Container, Row, Col} from 'reactstrap';

import Home from './Home';
import Previsio from './Previsio';
import Persones from './Persones';
import Detalle from './Detalle';
import NotFound from './NotFound';

export default () => (
  <BrowserRouter>
    <Container>

      <Row>
        <Col>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/previsio">Previsio</Link>
            </li>
            <li>
              <Link to="/persones">Persones</Link>
            </li>
          </ul>
        </Col>
      </Row>

      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/previsio" component={Previsio} />
        <Route path="/persones" component={Persones} />
        <Route path="/detalle" render={()=><h1>detalle</h1>} />
      s  <Route component={NotFound} />
      </Switch>
      
    </Container> 
  </BrowserRouter>
);
