import React from 'react';
import { Container, Col, Row, Button } from 'reactstrap';

const Cosa = (props) => {
    
    return (
        <Container>
            <Row>
                <Col xs="6" md="3">
                    <div className="cosa">
                        <p>{props.texto}</p>
                        <Button>Enviar</Button>
                    </div>
                </Col>
            </Row>
        </Container>
    ) ;
};

export default Cosa;