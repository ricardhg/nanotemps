import React from 'react';
import { Col, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button } from 'reactstrap';
import {  Link } from "react-router-dom";

import './css/persona.css';

const Persona = (props) => {
    
    return (
        <Col xs="6" md="4" >
            <Card>
                <CardImg top width="100%" src={props.data.picture.large} alt="Card image cap" />
                <CardBody>
                <CardTitle>{`${props.data.name.first} ${props.data.name.last}`}</CardTitle>
                <CardSubtitle>{props.data.email}</CardSubtitle>
                <CardText>
                    Location:<br/>
                        {props.data.location.street}<br/>
                        {props.data.location.city}<br/>
                        {props.data.location.state}<br/>
                        {props.data.location.postcode}<br/>
                </CardText>
                <Link to={"/detalle/"+props.data.idpersona} ><Button>Detalle</Button></Link>
                </CardBody>
            </Card>
        </Col>

    ) ;

};

export default Persona;