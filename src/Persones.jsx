import React from 'react';
import {Container, Row, Col} from 'reactstrap';

import Persona from './Persona';


// const API_KEY = "W110-98H2-IPME-3PDA";
// ricardhg 1234 randomapi.com

const URL = "https://randomuser.me/api/";


export default class Persones extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            persones: []
        };

        this.getPersones = this.getPersones.bind(this);
        this.getPersones();
    }

    getPersones(){
        const apiUrl = "https://randomuser.me/api/?results=10";
        
        fetch(apiUrl)
            .then(response => response.json())
            .then(datos => datos.results)
            .then(persones => {
                let num=1000;
                return persones.map(item => {
                    item.idpersona=num++;
                    return item;
                })        
            } )
            .then(persones => this.setState({persones: persones}))
            .catch(error => console.log(error));
    }

    render() {

        if (this.state.persones.length===0) {
            return <h1>Cargando datos...</h1>
        }

        let i = 1;
        let cards= this.state.persones.map(item  => <Persona key={i++} data={item} />);

        

        return (
            <>
           <Container>
               <Row>
                   {cards}
               </Row>
           </Container>

            </>
        );
    }

}